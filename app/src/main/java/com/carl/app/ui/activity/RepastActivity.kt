package com.carl.app.ui.activity

import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import com.carl.app.R
import com.carl.app.adapter.BaseRecyclerAdapter
import com.carl.app.adapter.SmartViewHolder
import com.carl.app.base.BaseActivity
import com.carl.app.bean.RxBusInfo
import com.carl.app.utils.LogUtils
import com.carl.app.utils.StatusBarUtil
import com.carl.app.utils.ToastUtils
import com.luck.picture.lib.rxbus2.RxBus
import com.luck.picture.lib.rxbus2.Subscribe
import com.luck.picture.lib.rxbus2.ThreadMode
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener
import java.util.*


/**
 * Created by carl on 2017/10/30.
 */

class RepastActivity : BaseActivity() {

    private open inner class Model {
        internal var imageId: Int = 0
        internal var avatarId: Int = 0
        internal var name: String? = null
        internal var nickname: String? = null
    }

    private var mAdapter: BaseRecyclerAdapter<Model>? = null


    override fun initImmersionBar() {
        super.initImmersionBar()
        mImmersionBar.statusBarDarkFont(true, 0.2f)
                .navigationBarColor(R.color.black)
                .init()


    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun eventBus(obj: RxBusInfo<String>) {

        LogUtils.i("TAG", obj.what.toString() + "  " + obj.data)
        /* switch(obj.what) {
            case 2770:

            default:
        }*/
    }

    /**
     * 是否沉浸式
     * @return
     */
    override fun isImmersionBarEnabled(): Boolean {
        return true
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_practice_repast
    }


    //
    override fun afterCreate(savedInstanceState: Bundle ?) {

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        //        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                finish();
        //            }
        //        });


        val refreshLayout = findViewById(R.id.refreshLayout) as RefreshLayout
        //初始化列表和监听
        val view = findViewById(R.id.recyclerView)
        if (view is RecyclerView) {
            val recyclerView = view
            recyclerView.layoutManager = LinearLayoutManager(applicationContext)
            recyclerView.itemAnimator = DefaultItemAnimator()
            mAdapter = object : BaseRecyclerAdapter<Model>(loadModels(), R.layout.listitem_practive_repast) {
                override fun onBindViewHolder(holder: SmartViewHolder, model: Model, position: Int) {
                    holder.text(R.id.name, model.name)
                    holder.text(R.id.nickname, model.nickname)
                    //                    holder.image(R.id.image, model.imageId);
                    //                    holder.image(R.id.avatar, model.avatarId);\
                }
            }
            recyclerView.setAdapter(mAdapter)

            refreshLayout.setOnRefreshLoadmoreListener(object : OnRefreshLoadmoreListener {
                override fun onRefresh(refreshlayout: RefreshLayout) {
                    refreshLayout.layout.postDelayed({
                        refreshlayout.finishRefresh()
                        refreshlayout.isLoadmoreFinished = false//恢复上拉状态
                    }, 2000)
                }

                override fun onLoadmore(refreshlayout: RefreshLayout) {
                    refreshLayout.layout.postDelayed({
                        mAdapter!!.loadmore(loadModels())
                        refreshlayout.finishLoadmore()
                        if (mAdapter!!.count > 12) {
                            ToastUtils.showLong("数据全部加载完毕")
                            refreshlayout.isLoadmoreFinished = true//设置之后，将不会再触发加载事件
                        }
                    }, 1000)
                }
            })
        }

        //状态栏透明和间距处理
        StatusBarUtil.darkMode(this)
        StatusBarUtil.setPaddingSmart(this, view)
        StatusBarUtil.setPaddingSmart(this, toolbar)
        StatusBarUtil.setPaddingSmart(this, findViewById(R.id.blurview))
        StatusBarUtil.setMargin(this, findViewById(R.id.clheader))



        handler.postDelayed({ RxBus.getDefault().post(RxBusInfo(111, "XXXX")) }, 3000)
    }

    internal var handler = Handler()

    /**
     * 模拟数据
     */
    private fun loadModels(): Collection<Model> {
        return Arrays.asList(
                object : Model() {
                    init {
                        this.name = "但家香酥鸭"
                        this.nickname = "爱过那张脸"
                        this.imageId = R.mipmap.image_practice_repast_1
                        this.avatarId = R.drawable.ic_camera
                    }
                }, object : Model() {
            init {
                this.name = "香菇蒸鸟蛋"
                this.nickname = "淑女算个鸟"
                this.imageId = R.mipmap.image_practice_repast_2
                this.avatarId = R.drawable.ic_camera
            }
        }, object : Model() {
            init {
                this.name = "花溪牛肉粉"
                this.nickname = "性感妩媚"
                this.imageId = R.mipmap.image_practice_repast_3
                this.avatarId = R.drawable.ic_camera
            }
        }, object : Model() {
            init {
                this.name = "破酥包"
                this.nickname = "一丝丝纯真"
                this.imageId = R.mipmap.image_practice_repast_4
                this.avatarId = R.drawable.ic_camera
            }
        })
    }
}
