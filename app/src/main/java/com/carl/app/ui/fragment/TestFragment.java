package com.carl.app.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.carl.app.R;
import com.carl.app.base.BaseFragment;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.mylhyl.circledialog.CircleDialog;
import com.mylhyl.circledialog.callback.ConfigButton;
import com.mylhyl.circledialog.callback.ConfigDialog;
import com.mylhyl.circledialog.params.ButtonParams;
import com.mylhyl.circledialog.params.DialogParams;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by carl on 2017/10/30.
 */

public class TestFragment extends BaseFragment {


    @BindView(R.id.tv_content)
    TextView tv_content;

    @OnClick(R.id.btn_photo)
    void btnClick(){
//
//        new CircleDialog.Builder((FragmentActivity) mActivity)
//                .setTitle("标题")
//                .setText("提示框")
//                .setPositive("确定", null)
//                .show();

        final String[] items = {"拍照", "从相册选择", "小视频"};
        new CircleDialog.Builder((FragmentActivity) mActivity)
                .configDialog(new ConfigDialog() {
                    @Override
                    public void onConfig(DialogParams params) {
                        //增加弹出动画
//                        params.animStyle = R.style.dialogWindowAnim;
                    }
                })
                .setTitle("标题")
                .setTitleColor(Color.BLUE)
                .setItems(items, new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                })
                .setNegative("取消", null)
                .configNegative(new ConfigButton() {
                    @Override
                    public void onConfig(ButtonParams params) {
                        //取消按钮字体颜色
                        params.textColor = Color.RED;
                    }
                })
                .show();




    }
    public static TestFragment newInstance(Bundle bundle) {
        TestFragment fragment = new TestFragment();
        if (null != bundle) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.statusBarDarkFont(true, 0.2f)
                .navigationBarColor(R.color.black)
                .init();
    }

    /**
     * 是否沉浸式
     * @return
     */
    @Override
    protected boolean isImmersionBarEnabled() {
        return false;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_test;
    }

    @Override
    protected void afterCreate(Bundle savedInstanceState) {

//        showErrorView();
        tv_content.setText(getArguments().getString("title"));
    }
}
