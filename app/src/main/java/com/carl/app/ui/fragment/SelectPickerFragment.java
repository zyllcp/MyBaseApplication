package com.carl.app.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.carl.app.R;
import com.carl.app.base.BaseFragment;
import com.carl.app.task.AddressPickTask;
import com.carl.app.utils.ToastUtils;
import com.mylhyl.circledialog.CircleDialog;
import com.mylhyl.circledialog.callback.ConfigButton;
import com.mylhyl.circledialog.callback.ConfigDialog;
import com.mylhyl.circledialog.params.ButtonParams;
import com.mylhyl.circledialog.params.DialogParams;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.addapp.pickers.entity.City;
import cn.addapp.pickers.entity.County;
import cn.addapp.pickers.entity.Province;
import cn.addapp.pickers.listeners.OnLinkageListener;
import cn.addapp.pickers.picker.AddressPicker;
import cn.addapp.pickers.util.ConvertUtils;
import cn.addapp.pickers.util.LogUtils;

/**
 * Created by carl on 2017/10/30.
 */

public class SelectPickerFragment extends BaseFragment {

    @BindView(R.id.tv_content)
    TextView tv_content;

    @OnClick(R.id.btn1)
    void btnClick1(View view) {
        onAddressPicker(view);
    }
    @OnClick(R.id.btn2)
    void btnClick2(View view) {
        onAddress2Picker(view);
    }
    @OnClick(R.id.btn3)
    void btnClick3(View view) {
        onAddress3Picker(view);
    }

    public static SelectPickerFragment newInstance(Bundle bundle) {
        SelectPickerFragment fragment = new SelectPickerFragment();
        if (null != bundle) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.statusBarDarkFont(true, 0.2f)
                .navigationBarColor(R.color.black)
                .init();
    }

    /**
     * 是否沉浸式
     *
     * @return
     */
    @Override
    protected boolean isImmersionBarEnabled() {
        return false;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_select_picker;
    }

    @Override
    protected void afterCreate(Bundle savedInstanceState) {
    }


    public void onAddressPicker(View view) {
        AddressPickTask task = new AddressPickTask(mActivity);
        task.setHideProvince(false);
        task.setHideCounty(false);
        task.setCallback(new AddressPickTask.Callback() {
            @Override
            public void onAddressInitFailed() {
                ToastUtils.showLong("数据初始化失败");
            }

            @Override
            public void onAddressPicked(Province province, City city, County county) {
                if (county == null) {
                    ToastUtils.showLong(province.getAreaName() + city.getAreaName());
                } else {
                    ToastUtils.showLong(province.getAreaName() + city.getAreaName() + county.getAreaName());
                }
            }
        });
        task.execute("重庆市", "重庆市", "万州");
    }

    public void onAddress2Picker(View view) {
        try {
            ArrayList<Province> data = new ArrayList<>();
            String json = ConvertUtils.toString(mActivity.getAssets().open("city2.json"));
            data.addAll(JSON.parseArray(json, Province.class));
            AddressPicker picker = new AddressPicker(mActivity, data);
            picker.setHideProvince(true);
            picker.setCanLoop(true);
            picker.setWheelModeEnable(true);
            picker.setSelectedItem("贵州", "贵阳", "花溪");
            picker.setOnLinkageListener(new OnLinkageListener() {
                @Override
                public void onAddressPicked(Province province, City city, County county) {
                    ToastUtils.showLong("province : " + province + ", city: " + city + ", county: " + county);
                }
            });
            picker.show();
        } catch (Exception e) {
            ToastUtils.showLong(LogUtils.toStackTraceString(e));
        }
    }


    public void onAddress3Picker(View view) {
        AddressPickTask task = new AddressPickTask(mActivity);
        task.setHideCounty(true);
        task.setCallback(new AddressPickTask.Callback() {
            @Override
            public void onAddressInitFailed() {
                ToastUtils.showLong("数据初始化失败");
            }

            @Override
            public void onAddressPicked(Province province, City city, County county) {
                ToastUtils.showLong(province.getAreaName() + " " + city.getAreaName());
            }
        });
        task.execute("四川", "阿坝");
    }

}
