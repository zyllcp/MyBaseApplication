package com.carl.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.carl.app.App;
import com.carl.app.R;
import com.carl.app.adapter.BaseRecyclerAdapter;
import com.carl.app.adapter.SmartViewHolder;
import com.carl.app.base.BaseFragment;
import com.carl.app.utils.StatusBarUtil;
import com.carl.app.utils.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;

import java.util.ArrayList;
import java.util.List;

import ezy.ui.layout.LoadingLayout;

/**
 * Created by carl on 2017/10/30.
 */

public class RepastFragment extends BaseFragment {
    public RepastFragment() {
    }

    private class Model {
        String imageId;
        int avatarId;
        String name;
        String nickname;
    }

    QuickAdapter adapter;
    BaseRecyclerAdapter<Model> adapterM;

    public static RepastFragment newInstance(Bundle bundle) {
        RepastFragment fragment = new RepastFragment();
        if (null != bundle) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.statusBarDarkFont(true, 0.2f)
                .navigationBarColor(R.color.black)
                .init();
    }

    /**
     * 是否沉浸式
     *
     * @return
     */
    @Override
    protected boolean isImmersionBarEnabled() {
        return false;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_practice_repast;
    }


    @Override
    protected void afterCreate(Bundle savedInstanceState) {
        final Toolbar toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
        final RefreshLayout refreshLayout = (RefreshLayout) mView.findViewById(R.id.refreshLayout);
        //初始化列表和监听
//
        final LoadingLayout loading = (LoadingLayout) mView.findViewById(R.id.loading);
//
        View view = mView.findViewById(R.id.recyclerView);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loading.showContent();
            }
        },5000);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                loading.showError();
//            }
//        },10000);
        loadModels();
//        adapter = new QuickAdapter(R.layout.listitem_practive_repast, App.getApplication());
        adapterM = new BaseRecyclerAdapter<Model>(R.layout.listitem_practive_repast) {

            @Override
            protected void onBindViewHolder(SmartViewHolder holder, Model model, int position) {

            }
        };

//        adapter.openLoadAnimation();
        if (view instanceof RecyclerView) {
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(mActivity.getApplicationContext()));
//            StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(
//                    2,
//                    StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            adapterM.refresh(loadModels());
            recyclerView.setAdapter(adapterM);
//            adapter.replaceData(loadModels());
//            recyclerView.setAdapter(adapter);
//            refreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
//                @Override
//                public void onRefresh(final RefreshLayout refreshlayout) {
//                    refreshLayout.getLayout().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            adapter.replaceData(loadModels());
//                            refreshlayout.finishRefresh();
//                            refreshlayout.setLoadmoreFinished(false);//恢复上拉状态
//                        }
//                    }, 2000);
//                }
//
//                @Override
//                public void onLoadmore(final RefreshLayout refreshlayout) {
//                    refreshLayout.getLayout().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            refreshlayout.finishLoadmore();
//                            adapter.addData(loadModels());
//                            if (adapter.getItemCount() > 100) {
//                                ToastUtils.showLong("数据全部加载完毕");
//                                refreshlayout.setLoadmoreFinished(true);//设置之后，将不会再触发加载事件
//                            }
//                        }
//                    }, 1000);
//                }
//            });
        }

        //状态栏透明和间距处理
        StatusBarUtil.darkMode(mActivity);
        StatusBarUtil.setPaddingSmart(mActivity, view);
        StatusBarUtil.setPaddingSmart(mActivity, toolbar);
        StatusBarUtil.setPaddingSmart(mActivity, mView.findViewById(R.id.blurview));
        StatusBarUtil.setMargin(mActivity, mView.findViewById(R.id.clheader));
//        StatusBarUtil.setMargin(mActivity, loading);
    }


    public class QuickAdapter extends BaseQuickAdapter<Model, BaseViewHolder> {
        Context mContext;

        public QuickAdapter(@LayoutRes int layoutResId, Context context) {
            super(layoutResId);
            mContext = context;
        }

        @Override
        protected void convert(BaseViewHolder baseViewHolder, Model model) {
            baseViewHolder.setText(R.id.name, model.name)
                    .setText(R.id.nickname, model.nickname);
            Glide.with(mContext).load(model.imageId).into((ImageView) baseViewHolder.getView(R.id.image));
            Glide.with(mContext).load(model.avatarId).into((ImageView) baseViewHolder.getView(R.id.avatar));
        }
    }


    /**
     * 模拟数据
     */
    private List<Model> loadModels() {
        List<Model> dataRel = new ArrayList<>();
        dataRel.add(new Model() {{
            this.name = "但家香酥鸭";
            this.nickname = "爱过那张脸";
            this.imageId = "http://imgsrc.baidu.com/imgad/pic/item/024f78f0f736afc3ca2a3143b919ebc4b74512f5.jpg";
            this.avatarId = R.drawable.ic_camera;
        }});
        dataRel.add(new Model() {{
            this.name = "香菇蒸鸟蛋";
            this.nickname = "淑女算个鸟";
            this.imageId = "http://imgsrc.baidu.com/imgad/pic/item/cefc1e178a82b9012eec632f798da9773912ef6d.jpg";
            this.avatarId = R.drawable.ic_camera;
        }});

        dataRel.add(new Model() {{
            this.name = "破酥包";
            this.nickname = "一丝丝纯真";
            this.imageId = "http://imgsrc.baidu.com/imgad/pic/item/4bed2e738bd4b31cf3cf359c8dd6277f9e2ff822.jpg";
            this.avatarId = R.drawable.ic_camera;
        }});
        dataRel.add(new Model() {{
            this.name = "花溪牛肉粉";
            this.nickname = "性感妩媚";
            this.imageId = "http://www.taopic.com/uploads/allimg/110801/110-110p10pj319.jpg";
            this.avatarId = R.drawable.ic_camera;
        }});
        dataRel.add(new Model() {{
            this.name = "但家香酥鸭";
            this.nickname = "爱过那张脸";
            this.imageId = "http://imgsrc.baidu.com/imgad/pic/item/024f78f0f736afc3ca2a3143b919ebc4b74512f5.jpg";
            this.avatarId = R.drawable.ic_camera;
        }});
        dataRel.add(new Model() {{
            this.name = "香菇蒸鸟蛋";
            this.nickname = "淑女算个鸟";
            this.imageId = "http://imgsrc.baidu.com/imgad/pic/item/cefc1e178a82b9012eec632f798da9773912ef6d.jpg";
            this.avatarId = R.drawable.ic_camera;
        }});

        dataRel.add(new Model() {{
            this.name = "破酥包";
            this.nickname = "一丝丝纯真";
            this.imageId = "http://imgsrc.baidu.com/imgad/pic/item/4bed2e738bd4b31cf3cf359c8dd6277f9e2ff822.jpg";
            this.avatarId = R.drawable.ic_camera;
        }});
        dataRel.add(new Model() {{
            this.name = "花溪牛肉粉";
            this.nickname = "性感妩媚";
            this.imageId = "http://www.taopic.com/uploads/allimg/110801/110-110p10pj319.jpg";
            this.avatarId = R.drawable.ic_camera;
        }});

        return dataRel;
    }
}