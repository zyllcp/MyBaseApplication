package com.carl.app.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CalendarView;

import com.carl.app.R;
import com.carl.app.base.BaseActivity;
import com.carl.app.utils.LogUtils;
import com.carl.app.utils.ToastUtils;

/**
 * Created by carl on 2017/12/22.
 */


public class TestKonlitActivity extends BaseActivity {


    private CalendarView mCalendarView;
    @Override
    protected boolean isImmersionBarEnabled() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_test_konlit;
    }

    @Override
    protected void afterCreate(Bundle savedInstanceState) {


    }

    private void initWidget(){

        mCalendarView = (CalendarView) findViewById(R.id.calendarView);

        if (mCalendarView != null) {

            mCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                    LogUtils.i(TestKonlitActivity.class.getSimpleName(),"year is : "+ year +" month : " +month+" dayOfMonth :" +dayOfMonth );
                    ToastUtils.showLong("year is : "+ year +" month : " +month+" dayOfMonth :" +dayOfMonth);

                }

            });

//            mCalendarView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                @Override
//                public void onFocusChange(View v, boolean hasFocus) {
//
//                }
//            });
//
//            mCalendarView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });

            mCalendarView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int mFirstDayofWeek = mCalendarView.getFirstDayOfWeek() ;
                    long getDataValue = mCalendarView.getDate();
                    LogUtils.i(TestKonlitActivity.class.getSimpleName(),"mfirstDayOfWeek values is : " +mFirstDayofWeek +" and getdata is :"+ getDataValue);


//                    LogUtils.i(TestKonlitActivity.class.getSimpleName(),"year is : "+ year +" month : " +month+" dayOfMonth :" +dayOfMonth );
//                    ToastUtils.showLong("year is : "+ year +" month : " +month+" dayOfMonth :" +dayOfMonth);
                }
            });
        }
    }
}
