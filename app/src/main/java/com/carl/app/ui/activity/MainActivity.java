package com.carl.app.ui.activity;

import android.os.Bundle;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.carl.app.R;
import com.carl.app.base.BaseTabBottomActivity;
import com.carl.app.ui.fragment.HomeFragment;
import com.carl.app.ui.fragment.RepastFragment;
import com.carl.app.ui.fragment.SelectPickerFragment;
import com.carl.app.ui.fragment.TestFragment;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.ui.BetaActivity;


public class MainActivity extends BaseTabBottomActivity {


    @Override
    protected void afterCreate(Bundle savedInstanceState) {


        /***
         * CarlBranch
         */
        Beta.checkUpgrade();
        Bundle home = new Bundle();
        Bundle home1 = new Bundle();
        Bundle home2 = new Bundle();
        home.putString("title", "tab1");
        home1.putString("title", "tab2");
        home2.putString("title", "tab3");
        addItem(HomeFragment.newInstance(home), R.mipmap.ic_launcher, "tab1", R.color.colorPrimary);
        addItem(TestFragment.newInstance(home1), R.mipmap.ic_launcher_round, "tb2", R.color.colorPrimaryDark);
        addItem(SelectPickerFragment.newInstance(home2), R.mipmap.ic_launcher, "tb3", R.color.colorAccent);
        addItem(RepastFragment.newInstance(home2), R.mipmap.ic_launcher, "tb4", R.color.colorAccent);

        setNavBarStyle(BottomNavigationBar.MODE_FIXED, BottomNavigationBar.BACKGROUND_STYLE_STATIC);
        initialise(R.id.layFrame);
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }

    /***
     *  初始化沉浸式
     * @return
     */
    @Override
    protected boolean isImmersionBarEnabled() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }
}
