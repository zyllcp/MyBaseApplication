package com.carl.app.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.carl.app.R;
import com.carl.app.base.BaseFragment;
import com.carl.app.ui.activity.RepastActivity;
import com.carl.app.ui.activity.TestKonlitActivity;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by carl on 2017/10/30.
 */

public class HomeFragment extends BaseFragment {
    @OnClick(R.id.btn_photo)
    void btnClick() {
        goActivity(true,null,TestKonlitActivity.class);
//        startActivity(new Intent(mActivity, RepastActivity.class));
     /*   PictureSelector.create(getActivity())
                .openGallery(PictureMimeType.ofAll())
                .forResult(PictureConfig.CHOOSE_REQUEST);*/

    }

    public static HomeFragment newInstance(Bundle bundle) {
        HomeFragment fragment = new HomeFragment();
        if (null != bundle) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.statusBarDarkFont(true, 0.2f)
                .navigationBarColor(R.color.black)
                .init();
    }

    /**
     * 是否沉浸式
     *
     * @return
     */
    @Override
    protected boolean isImmersionBarEnabled() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void afterCreate(Bundle savedInstanceState) {

    }
}
