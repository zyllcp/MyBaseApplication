package com.carl.app.bean;

import java.io.Serializable;

/**
 * Created by carl on 2017/10/30.
 */

public class RxBusInfo<T>  implements Serializable {

    public int what;
    public T data;

    public int getWhat() {
        return what;
    }

    public RxBusInfo(int what, T data) {
        this.what = what;
        this.data = data;
    }

    public  RxBusInfo(){};
}
