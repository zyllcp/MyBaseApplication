package com.carl.app;

import android.app.Application;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;

import com.carl.app.ui.activity.MainActivity;
import com.carl.app.utils.FileUtils;
import com.carl.app.utils.LogUtils;
import com.squareup.leakcanary.LeakCanary;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;


/**
 * Created by Carl on 2017/4/17.
 * 自定义Application
 */

public class App extends Application {

    private static App mContext;
    public static final String APP_ID = "ceeb77905e";
    private static Handler mMainThreadHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);

        mContext = this;
        mMainThreadHandler = new Handler();

        //设置是否打印日志
        LogUtils.setIsLog(BuildConfig.LOG_DEBUG);

        //在6.0(M)版本下直接创建应用对应的文件夹
        //在6.0(M)版本以上的需要先进行权限申请
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            FileUtils.init(this);
        }

        Bugly.init(getApplicationContext(), APP_ID, false);

    }

    public static App getApplication() {
        return mContext;
    }

    public static Handler getMainThreadHandler() {
        return mMainThreadHandler;
    }
}
