package com.carl.app.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carl.app.App;
import com.gyf.barlibrary.ImmersionBar;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Car on 2017/4/18.
 * Fragment基类
 */

public abstract class BaseFragment extends Fragment {

    public View mView;

    private Unbinder unbinder;

    protected Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(getLayoutId(), container, false);
        unbinder = ButterKnife.bind(this, mView);
        afterCreate(savedInstanceState);
        if (isImmersionBarEnabled())
            initImmersionBar();

        return mView;
    }

    protected ImmersionBar mImmersionBar;

    /**
     * 初始化沉浸式
     */
    protected void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this);
        mImmersionBar.init();
    }

    protected abstract boolean isImmersionBarEnabled();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    protected abstract int getLayoutId();

    protected abstract void afterCreate(Bundle savedInstanceState);

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }


    /***
     *
     * @param isAnimator    是否设置跳转动画
     * @param animatorRes   动画资源
     * @param bundle        bundle 传值
     * @param fromActivty   需要跳转到的activity
     */

    public void goActivity(boolean isAnimator,int animatorRes,Bundle bundle,Class fromActivty){

        Intent intent = new Intent(App.getApplication(),fromActivty);

        if(bundle !=null){

            startActivity(intent,bundle);

        }else {

            startActivity(intent);

        }

        //是否开启跳转动画
        if(isAnimator){

            if(animatorRes > 0){
                //设置跳转动画

            }else if(animatorRes == -995995){
                //如果开启动画效果的同时,动画资源为-995995则说明利用代码的形式设置跳转动画
            }else {

                startAnimotor();
            }
        }
    }

    private void startAnimotor(){

       mActivity.overridePendingTransition(com.yalantis.ucrop.R.anim.abc_fade_in, com.yalantis.ucrop.R.anim.abc_fade_out);
    }

    public void goActivity(Bundle bundle,Class goActivity){
        this.goActivity(false,0,bundle,goActivity);
    }

    public void goActivity(boolean isAnimator,Bundle bundle,Class goActivity){
        goActivity(false,-995995,bundle,goActivity);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mImmersionBar != null)
            mImmersionBar.destroy();
    }
}
