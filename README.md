# MyBaseApplication

    /* View注解 ButterKnife */
    compile 'com.jakewharton:butterknife:8.5.1'
    annotationProcessor 'com.jakewharton:butterknife-compiler:8.5.1'
    /*图片选择器*/
    compile 'com.github.LuckSiege.PictureSelector:picture_library:v2.1.5'
    /*底部导航栏*/
    compile 'com.ashokvarma.android:bottom-navigation-bar:1.4.1'

    /*leakcanary 检查内存溢出*/
    debugCompile 'com.squareup.leakcanary:leakcanary-android:1.5.4'
    releaseCompile 'com.squareup.leakcanary:leakcanary-android-no-op:1.5.4'

    /*bugly 热更新*/
    compile 'com.tencent.bugly:crashreport_upgrade:1.3.1'
    compile 'com.tencent.bugly:nativecrashreport:3.3.1'


    /*ImmersionBar android4.4以上沉浸式状态栏和导航栏实现以及Bar的其他管理
    *http://www.jianshu.com/p/2a884e211a62
    * */
    compile 'com.gyf.barlibrary:barlibrary:2.3.0'

    /* 仿IOS Dialog 提示框 https://github.com/mylhyl/Android-CircleDialog*/
    compile 'com.mylhyl:circleDialog:2.1.6'

    /*选择器https://github.com/addappcn/android-pickers*/
    compile 'com.github.addappcn:android-pickers:1.0.1'
    /*fastjson*/
    compile 'com.alibaba:fastjson:1.1.56.android'

    /*loding layout */

    compile 'com.github.czy1121:loadinglayout:1.0.1'

    /*各种下拉刷新上拉加载更多 */
//    compile 'com.scwang.smartrefresh:SmartRefreshLayout:1.0.3'
//    compile 'com.scwang.smartrefresh:SmartRefreshHeader:1.0.3'//没有使用特殊Header，可以不加这行

    compile 'com.scwang.smartrefresh:SmartRefreshLayout:1.0.4-alpha-9'


    /*圆形图标*/
    compile 'de.hdodenhof:circleimageview:2.1.0'

    /*万能adapter*/

    compile 'com.github.CymChad:BaseRecyclerViewAdapterHelper:2.9.33'
    /*模糊化*/
    compile 'com.github.mmin18:realtimeblurview:1.0.6'

    /*logind view */
    compile 'com.wang.avi:library:2.1.3'
    /*动画 recyclerviw*/
    compile 'jp.wasabeef:recyclerview-animators:2.2.6'
